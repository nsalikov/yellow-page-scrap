# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import Join, TakeFirst, Compose, MapCompose
import re
import json


def filter_nonalpha(x):
    return x if not re.match('^[,\s]*$', x) else None


def translate_rating(rating):
    d = {"one": 1, "two": 2, "three": 3, "four": 4, "five": 5}
    for r in rating.split(' '):
        if r in d:
            return d[r]
    return 0


def extract_from_json_text(json_text):
    try:
        j = json.loads(json_text)
        path = j['fullImagePath']
    except:
        return

    return path


class YellowPageScrapItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    YID = scrapy.Field()
    Name = scrapy.Field(
        output_processor=TakeFirst(),
    )
    FullAddress = scrapy.Field(
        output_processor=TakeFirst(),
    )
    Address = scrapy.Field(
        output_processor=TakeFirst(),
    )
    City = scrapy.Field(
        output_processor=TakeFirst(),
    )
    State = scrapy.Field(
        output_processor=TakeFirst(),
    )
    Country = scrapy.Field(
        output_processor=TakeFirst(),
    )
    Phones = scrapy.Field(
        #
    )
    Zipcode = scrapy.Field(
        output_processor=TakeFirst(),
    )
    Categories = scrapy.Field(
        input_processor=MapCompose(filter_nonalpha),
    )
    Description = scrapy.Field(
        output_processor=TakeFirst(),
    )
    Hours = scrapy.Field(
        output_processor=lambda l: list(zip(l[::2], l[1::2]))
    )
    Services = scrapy.Field(
        output_processor=TakeFirst(),
    )
    Reviews = scrapy.Field(
        #
    )
    Rating = scrapy.Field(
        input_processor=MapCompose(translate_rating),
        output_processor=TakeFirst(),
    )
    Url = scrapy.Field(
        output_processor=TakeFirst(),
    )
    Website = scrapy.Field(
        output_processor=TakeFirst(),
    )
    ImageUrls = scrapy.Field(
        input_processor=MapCompose(extract_from_json_text),
        output_processor=Compose(lambda l: list(set(l))),
    )
    Images = scrapy.Field()


    def __repr__(self):
        d = {}

        if 'Name' in self:
            d['Name'] = self["Name"]
        if 'FullAddress' in self:
            d['FullAddress'] = self['FullAddress']
        if 'Phones' in self:
            d['Phones'] = self['Phones']
        if 'Website' in self:
            d['Website'] = self['Website']
        d['Url'] = self['Url']

        return repr(d)

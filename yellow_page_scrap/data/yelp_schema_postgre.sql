--- psql -h hostname -d databasename -U username -f file.sql
--- psql -d postgres -U postgres -f yelp_schema_postgre.sql

CREATE TABLE IF NOT EXISTS business (
  id BIGSERIAL PRIMARY KEY,
  yid INT NOT NULL UNIQUE,
  name VARCHAR(100) NULL,
  fulladdress VARCHAR(100) NULL,
  address VARCHAR(45) NULL,
  city VARCHAR(30) NULL,
  state CHAR(2) NULL,
  country VARCHAR(20) NULL,
  zipcode INT NULL,
  service VARCHAR(255) NULL,
  description TEXT NULL,
  rating INT NULL,
  website VARCHAR(255) NULL,
  url VARCHAR(255) NULL,
  created TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  updated TIMESTAMP NULL DEFAULT NULL);


CREATE TABLE IF NOT EXISTS phone (
  id BIGSERIAL PRIMARY KEY,
  yid INT NOT NULL,
  phone VARCHAR(20) NULL,
  CONSTRAINT yid
    FOREIGN KEY (yid)
    REFERENCES business (yid)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE TABLE IF NOT EXISTS hour (
  id BIGSERIAL PRIMARY KEY,
  yid INT NOT NULL,
  day VARCHAR(20) NULL,
  hour VARCHAR(45) NULL,
  CONSTRAINT yip
    FOREIGN KEY (yid)
    REFERENCES business (yid)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE TABLE IF NOT EXISTS review (
  id BIGSERIAL PRIMARY KEY,
  yid INT NOT NULL,
  review TEXT NULL,
  CONSTRAINT yid
    FOREIGN KEY (yid)
    REFERENCES business (yid)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE TABLE IF NOT EXISTS image (
  id BIGSERIAL PRIMARY KEY,
  yid INT NOT NULL,
  url VARCHAR(255) NULL,
  path VARCHAR(255) NULL,
  checksum CHAR(32) NULL,
  CONSTRAINT yid
    FOREIGN KEY (yid)
    REFERENCES business (yid)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE TABLE IF NOT EXISTS category (
  id BIGSERIAL PRIMARY KEY,
  yid INT NOT NULL,
  category VARCHAR(255) NULL,
  CONSTRAINT yid
    FOREIGN KEY (yid)
    REFERENCES business (yid)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE INDEX business_yid_index ON business (yid);
CREATE INDEX phone_yid_index ON phone (yid);
CREATE INDEX hour_yid_index ON hour (yid);
CREATE INDEX review_yid_index ON review (yid);
CREATE INDEX image_yid_index ON image (yid);
CREATE INDEX category_yid_index ON category (yid);

CREATE OR REPLACE FUNCTION update_timestamp_column()
RETURNS TRIGGER AS $$
BEGIN
   NEW.updated := now();
   RETURN NEW;
END;
$$ language 'plpgsql';


CREATE TRIGGER update_business_timestamps BEFORE UPDATE
    ON business FOR EACH ROW EXECUTE PROCEDURE
    update_timestamp_column();


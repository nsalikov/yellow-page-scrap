# -*- coding: utf-8 -*-
import scrapy
from scrapy.exceptions import NotConfigured
from scrapy.loader import ItemLoader
from yellow_page_scrap.items import YellowPageScrapItem
import json
import re


class YelpSpider(scrapy.Spider):
    name = 'yelp'
    allowed_domains = ['yellowpages.com']
    url = 'https://www.yellowpages.com/search?search_terms={category}&geo_location_terms={geo}'


    def start_requests(self):
        if 'LOCATIONS' not in self.settings:
            raise NotConfigured("You must specificy LOCATIONS' file")
        if 'CATEGORIES' not in self.settings:
            raise NotConfigured("You must specificy CATEGORIES' file")

        categories = [line.strip() for line in open(self.settings['CATEGORIES'], 'r', encoding='utf8')]
        locations = [line.strip() for line in open(self.settings['LOCATIONS'], 'r', encoding='utf8')]

        for category in categories:
            for location in locations:
                yield scrapy.Request(self.url.format(category=category, geo=location),
                                            callback=self.parse,
                                            errback=self.make_new_search_request)


    def make_new_search_request(self, failure):
        self.logger.info('Make new request <{url}>'.format(url=failure.request.url))
        return scrapy.Request(url=failure.request.url, callback=self.parse, errback=self.make_new_search_request, dont_filter=True)


    def make_new_item_request(self, failure):
        self.logger.info('Make new item request <{url}>'.format(url=failure.request.url))
        return scrapy.Request(url=failure.request.url, callback=self.parse_item, errback=self.make_new_item_request, dont_filter=True)


    def parse(self, response):
        for link in response.css('div.pagination li a ::attr(href)').extract():
            yield response.follow(link, callback=self.parse, errback=self.make_new_search_request)

        for link in response.css('div.scrollable-pane div.result div.info > h2 > a ::attr(href)'):
            yield response.follow(link, callback=self.parse_item, errback=self.make_new_item_request)


    def parse_item(self, response):
        l = ItemLoader(item=YellowPageScrapItem(), response=response)

        l.add_css('Name', 'h1 *::text')
        l.add_css('FullAddress', 'p.adr *::text')
        l.add_value('Country', 'United States')
        l.add_css('Phones', 'div.contact p.phone *::text')
        l.add_css('Categories', 'dd.categories span *::text')
        l.add_css('Description', 'dd.general-info *::text')
        l.add_css('Hours', 'div.open-details time *::text')
        l.add_xpath('Services', '//*[@id="business-info"]/dl/dt[text()="Services/Products"]/following-sibling::dd[1]/text()')
        l.add_css('Reviews', 'div#yp-reviews-container div.review-response p *::text')
        l.add_css('Rating', 'section.primary-info section.ratings div.rating-indicator ::attr(class)')
        l.add_css('Website', 'div.business-card-footer > a.website-link ::attr(href)')
        l.add_value('Url', response.url)
        l.add_css('ImageUrls', '*[data-media] ::attr(data-media)')

        return l.load_item()

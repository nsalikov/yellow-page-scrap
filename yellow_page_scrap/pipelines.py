# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import re
import psycopg2
from twisted.enterprise import adbapi
from scrapy.exceptions import DropItem


class UpdateItemInfoPipeline(object):

    def process_item(self, item, spider):
        # davis-drug-company-inc-480266489?lid=1001707172618
        # -(\d+)\?lid=(\d+)$
        match = re.search('-(\d+)\?', item['Url'], re.DOTALL)

        if match:
            item['YID'] = match.group(1)

        if 'FullAddress' not in item or not item['FullAddress']:
            return item

        address, city, state, zipcode = self.parse_address(item['FullAddress'])

        item['Address'] = address
        item['City'] = city
        item['State'] = state
        item['Zipcode'] = zipcode

        if not any((item['Address'], item['City'], item['State'])):
            spider.logger.debug("Could not parse store's address: {}".format(item['FullAddress']))

        return item


    def parse_address(self, address):
        # 604 E Franklin Blvd, Gastonia, NC 28054
        match = re.match('^\s*(.*)\s*,\s*(.*)\s*,\s*([A-Z]{2})\s*(\d+)\s*$', address, re.DOTALL)

        if match:
            return (match.group(1), match.group(2), match.group(3), match.group(4))

        # 1021 Us Highway 431, Anniston, AL
        match = re.match('^\s*(.*)\s*,\s*(.*)\s*,\s*([A-Z]{2})\s*$', address, re.DOTALL)

        if match:
            return (match.group(1), match.group(2), match.group(3), None)

        return (None, None, None, None)


class DuplicatesPipeline(object):

    def __init__(self):
        self.seen = set()


    def process_item(self, item, spider):
        # YID or Url?
        return self.filter_by_key(item, spider, 'YID')


    def filter_by_key(self, item, spider, key):
        if key not in item or not item[key]:
            raise DropItem("Can't not found {}:".format(key))
        if item[key] in self.seen:
            raise DropItem("Duplicate {}:".format(key))
        else:
            self.seen.add(item[key])
            return item


class PostgresPipeline(object):

    business =  """INSERT INTO business (yid, name, fulladdress, address, city, state, country, zipcode, service, description, rating, website, url)
                   VALUES (
                            %(yid)s, %(name)s, %(fulladdress)s, %(address)s, %(city)s, %(state)s, %(country)s, %(zipcode)s,
                            left(%(service)s, 255),
                            %(description)s, %(rating)s, %(website)s, %(url)s
                   )
                   ON CONFLICT (yid) DO UPDATE SET
                   (name, fulladdress, address, city, state, country, zipcode, service, description, rating, website, url) =
                   (EXCLUDED.name, EXCLUDED.fulladdress, EXCLUDED.address, EXCLUDED.city, EXCLUDED.state, EXCLUDED.country,
                   EXCLUDED.zipcode, EXCLUDED.service, EXCLUDED.description, EXCLUDED.rating, EXCLUDED.website, EXCLUDED.url);
                """

    category =  """INSERT INTO category (yid, category)
                   VALUES (%(yid)s, %(category)s);"""

    hour =      """INSERT INTO hour (yid, day, hour)
                   VALUES (%(yid)s, %(day)s, %(hour)s);"""

    image =     """INSERT INTO image (yid, url, path, checksum)
                   VALUES (%(yid)s, %(url)s, %(path)s, %(checksum)s);"""

    phone =     """INSERT INTO phone (yid, phone)
                   VALUES (%(yid)s, %(phone)s);"""

    review =    """INSERT INTO review (yid, review)
                   VALUES (%(yid)s, %(review)s);"""


    def __init__(self, dbpool):
    	self.dbpool = dbpool


    @classmethod
    def from_settings(cls, settings):
        dbargs = dict(
            host=settings['POSTGRESQL_HOST'],
            database=settings['POSTGRESQL_DBNAME'],
            user=settings['POSTGRESQL_USER'],
            password=settings['POSTGRESQL_PASSWD'],
        )

        dbpool = adbapi.ConnectionPool('psycopg2', **dbargs)
        return cls(dbpool)


    def process_item(self, item, spider):
        # run db query in the thread pool
        d = self.dbpool.runInteraction(self._do_upsert, item, spider)
        d.addErrback(self._handle_error, item, spider)

        # at the end return the item in case of success or failure
        d.addBoth(lambda _: item)

        # return the deferred instead the item. This makes the engine to
        # process next item (according to CONCURRENT_ITEMS setting) after this
        # operation (deferred) has finished.
        return d


    def _handle_error(self, failure, item, spider):
        spider.logger.error(failure)


    def _do_upsert(self, conn, item, spider):

        yid = item['YID']
        d = {'yid': yid,
            'name': item['Name'] if 'Name' in item else None,
            'fulladdress': item['FullAddress'] if 'FullAddress' in item else None,
            'address': item['Address'] if 'Address' in item else None,
            'city': item['City'] if 'City' in item else None,
            'state': item['State'] if 'State' in item else None,
            'country': item['Country'] if 'Country' in item else None,
            'zipcode': item['Zipcode'] if 'Zipcode' in item else None,
            'service': item['Services'] if 'Services' in item else None,
            'description': item['Description'] if 'Description' in item else None,
            'rating': item['Rating'] if 'Rating' in item else None,
            'website': item['Website'] if 'Website' in item else None,
            'url': item['Url'],
        }

        conn.execute(self.business, d)

        if 'Categories' in item and item['Categories']:
            conn.execute('DELETE FROM category WHERE yid = %(yid)s;', {'yid': yid})
            conn.executemany(self.category, [{'yid': yid, 'category': c} for c in item['Categories']])

        if 'Hours' in item and item['Hours']:
            conn.execute('DELETE FROM hour WHERE yid = %(yid)s;', {'yid': yid})
            conn.executemany(self.hour, [{'yid': yid, 'day': h[0], 'hour': h[1]} for h in item['Hours']])

        if 'Images' in item and item['Images']:
            conn.execute('DELETE FROM image WHERE yid = %(yid)s;', {'yid': yid})
            conn.executemany(self.image, [{'yid': yid, 'url': i['url'], 'path': i['path'], 'checksum': i['checksum']} for i in item['Images']])

        if 'Phones' in item and item['Phones']:
            conn.execute('DELETE FROM phone WHERE yid = %(yid)s;', {'yid': yid})
            conn.executemany(self.phone, [{'yid': yid, 'phone': p} for p in item['Phones']])

        if 'Reviews' in item and item['Reviews']:
            conn.execute('DELETE FROM review WHERE yid = %(yid)s;', {'yid': yid})
            conn.executemany(self.review, [{'yid': yid, 'review': r} for r in item['Reviews']])

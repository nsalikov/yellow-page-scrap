# Yellowpages' Scraper

Intelligent scraper for [yellowpages.com](https://www.yellowpages.com).

## Getting Started

### Prerequisites

* [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [python 3](https://docs.python.org/3/using/index.html)
* [scrapy](https://doc.scrapy.org/en/latest/intro/install.html)
* [shub](https://helpdesk.scrapinghub.com/support/solutions/articles/22000204081-deploying-your-spiders-to-scrapy-cloud) (optionally, for deployment to scrapinghub.com)

### Installing

Clone project:

```
http://nikolai@stash.denovolab.com/scm/cal1800/yellow-page-scrap.git
```

Change directory and install requirements:

```
cd yellow-page-scrap
pip install -r requirements.txt
```

If you have both python 2 and python 3 installed, use pip3 instead:

```
cd yellow-page-scrap
pip3 install -r requirements.txt
```

### Configuration

#### Initial configuration

Setup this variables in `settings.py`:

```
PROXY_LIST = 'data/proxies.txt'
CATEGORIES = 'data/categories.txt'
LOCATIONS = 'data/locations.txt'

IMAGES_STORE = '/home/nikolai/pics'

POSTGRESQL_HOST = 'localhost'
POSTGRESQL_DBNAME = 'postgres'
POSTGRESQL_USER = 'postgres'
POSTGRESQL_PASSWD = ''
```

#### Proxies

In order to use proxies, you need to prepare file with list of proxies:

```
# Proxy list containing entries like
# http://host1:port
# http://username:password@host2:port
# http://host3:port

PROXY_LIST = 'data/proxies.txt'
```

Then uncomment last two lines in `settings.py`:

```
DOWNLOADER_MIDDLEWARES = {
    'scrapy_fake_useragent.middleware.RandomUserAgentMiddleware': 80,
    'scrapy.downloadermiddlewares.retry.RetryMiddleware': 90,
    # 'scrapy_proxies.RandomProxy': 100,
    # 'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 110,
}
```

Unfortunately, public proxies has been proved themselves pretty unrelaible (even pool of 300+ proxies). So it will be better to pick up 5-10 private proxies. To obtain it, you need to purchase some paid proxies with IP rotation (e.g. [proxymesh.com](http://proxymesh.com/)), install proxy servers on your servers/instances or use Tor ([like this](https://github.com/matejbasic/PythonScrapyBasicSetup)).

#### Increase Concurrency

To speed things up you need to [configure](https://doc.scrapy.org/en/latest/topics/settings.html) Scrapy through `settings.py` file.

```
CONCURRENT_REQUESTS = 80
DOWNLOAD_DELAY = 0.3
CONCURRENT_REQUESTS_PER_DOMAIN = 16
CONCURRENT_REQUESTS_PER_IP = 5

AUTOTHROTTLE_ENABLED = True
AUTOTHROTTLE_START_DELAY = 0.5
AUTOTHROTTLE_MAX_DELAY = 60
AUTOTHROTTLE_TARGET_CONCURRENCY = 5.0
```
You can increase request concurrency by increasing AUTOTHROTTLE_TARGET_CONCURRENCY and CONCURRENT_REQUESTS_PER_DOMAIN/CONCURRENT_REQUESTS_PER_IP (see [here](https://doc.scrapy.org/en/latest/topics/autothrottle.html)). Simultaneously, you can reduce (`DOWNLOAD_DELAY = 0.3`) or even disable (`DOWNLOAD_DELAY = 0`) download delay.

### Usage

Just run:

```
scrapy crawl yelp
```

To enable [persistence support](https://doc.scrapy.org/en/latest/topics/jobs.html) you need to define job directory:

```
scrapy crawl yelp -s JOBDIR=yellow_page_scrap/data/var/job/yelp
```

## Appendix

### Systemd unit example

```
[Unit]
Description=Scrapy-yelp service
After=network.target

[Service]
User=nikolai
Group=nikolai
Environment="now=$(date +%%Y-%%m-%%d.%%H:%%M:%%S)"
WorkingDirectory=/home/nikolai/projects/yellow-page-scrap/
ExecStart=/bin/bash -c "/usr/local/bin/scrapy crawl yelp --logfile=/home/nikolai/projects/yellow-page-scrap/yellow_page_scrap/var/log/yelp-${now}.log -s JOBDIR=/home/nikolai/projects/yellow-page-scrap/yellow_page_scrap/var/job/"
Restart=on-failure
RestartSec=180s

[Install]
WantedBy=multi-user.target
```

### PostgreSQL schema

See `yelp_schema_postgre.sql` in `data` directory.

```
CREATE TABLE IF NOT EXISTS business (
  id BIGSERIAL PRIMARY KEY,
  yid INT NOT NULL UNIQUE,
  name VARCHAR(100) NULL,
  fulladdress VARCHAR(100) NULL,
  address VARCHAR(45) NULL,
  city VARCHAR(30) NULL,
  state CHAR(2) NULL,
  country VARCHAR(20) NULL,
  zipcode INT NULL,
  service VARCHAR(255) NULL,
  description TEXT NULL,
  rating INT NULL,
  website VARCHAR(255) NULL,
  url VARCHAR(255) NULL,
  created TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  updated TIMESTAMP NULL DEFAULT NULL);


CREATE TABLE IF NOT EXISTS phone (
  id BIGSERIAL PRIMARY KEY,
  yid INT NOT NULL,
  phone VARCHAR(20) NULL,
  CONSTRAINT yid
    FOREIGN KEY (yid)
    REFERENCES business (yid)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE TABLE IF NOT EXISTS hour (
  id BIGSERIAL PRIMARY KEY,
  yid INT NOT NULL,
  day VARCHAR(20) NULL,
  hour VARCHAR(45) NULL,
  CONSTRAINT yip
    FOREIGN KEY (yid)
    REFERENCES business (yid)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE TABLE IF NOT EXISTS review (
  id BIGSERIAL PRIMARY KEY,
  yid INT NOT NULL,
  review TEXT NULL,
  CONSTRAINT yid
    FOREIGN KEY (yid)
    REFERENCES business (yid)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE TABLE IF NOT EXISTS image (
  id BIGSERIAL PRIMARY KEY,
  yid INT NOT NULL,
  url VARCHAR(255) NULL,
  path VARCHAR(255) NULL,
  checksum CHAR(32) NULL,
  CONSTRAINT yid
    FOREIGN KEY (yid)
    REFERENCES business (yid)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE TABLE IF NOT EXISTS category (
  id BIGSERIAL PRIMARY KEY,
  yid INT NOT NULL,
  category VARCHAR(255) NULL,
  CONSTRAINT yid
    FOREIGN KEY (yid)
    REFERENCES business (yid)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE INDEX business_yid_index ON business (yid);
CREATE INDEX phone_yid_index ON phone (yid);
CREATE INDEX hour_yid_index ON hour (yid);
CREATE INDEX review_yid_index ON review (yid);
CREATE INDEX image_yid_index ON image (yid);
CREATE INDEX category_yid_index ON category (yid);


CREATE OR REPLACE FUNCTION update_timestamp_column()
RETURNS TRIGGER AS $$
BEGIN
   NEW.updated := now();
   RETURN NEW;
END;
$$ language 'plpgsql';


CREATE TRIGGER update_business_timestamps BEFORE UPDATE
    ON business FOR EACH ROW EXECUTE PROCEDURE
    update_timestamp_column();

```

### Scrapinghub.com?

According to scrapinghub's [pricing](https://scrapinghub.com/scrapy-cloud), free account allow you to get 1 concurrent crawl. So you need to buy access to [expensive](https://scrapinghub.com/crawlera) Crawlera downloader for more concurrency. I don't recommend this.
